﻿using LogsProject.Data;
using System.Collections.Generic;
/**
* @author jose.sanchezt@campusucc.edu.co
*
* @date - $time$ 
*/
namespace LogsProject.Facades
{
    public class ClientFacade
    {
        public static List<Client> ClientList = new List<Client>();

        public static List<Client> CreateClientList()
        {
            ClientList.Add(new Client()
            {
                Id = "2341234213",
                FirstName = "Pedro",
                LastName = "Arias",
                ClientType = "natural",
                IdType = "CC",
                AccountNumber = "8412421"
            });
            ClientList.Add(new Client()
            {
                Id = "986462039",
                FirstName = "Elizabeth",
                LastName = "Olsen",
                ClientType = "natural",
                IdType = "CE",
                AccountNumber = "2123731"
            });
            ClientList.Add(new Client()
            {
                Id = "324726500",
                FirstName = "Daniel",
                LastName = "Delgado",
                ClientType = "natural",
                IdType = "CC",
                AccountNumber = "43512421"
            });
            ClientList.Add(new Client()
            {
                Id = "915311955",
                FirstName = "Darren",
                LastName = "Hays",
                ClientType = "natural",
                IdType = "CE",
                AccountNumber = "66412421"
            });
            ClientList.Add(new Client()
            {
                Id = "973387979",
                FirstName = "Dan",
                LastName = "Taylor",
                ClientType = "natural",
                IdType = "CE",
                AccountNumber = "23512421"
            });
            ClientList.Add(new Client()
            {
                Id = "860.029.924-7",
                FirstName = "UCC",
                LastName = "UCC",
                ClientType = "juridico",
                IdType = "NIT",
                AccountNumber = "345412421"
            });
            ClientList.Add(new Client()
            {
                Id = "236456529",
                FirstName = "Katherine",
                LastName = "Luna",
                ClientType = "natural",
                IdType = "CC",
                AccountNumber = "152512421"
            });
            ClientList.Add(new Client()
            {
                Id = "508990853",
                FirstName = "Michael",
                LastName = "Diaz",
                ClientType = "natural",
                IdType = "CC",
                AccountNumber = "5424112421"
            });
            ClientList.Add(new Client()
            {
                Id = "67472558",
                FirstName = "Doering",
                LastName = "Maria",
                ClientType = "natural",
                IdType = "CC",
                AccountNumber = "34122421"
            });

            return ClientList;
        }
    }
}
