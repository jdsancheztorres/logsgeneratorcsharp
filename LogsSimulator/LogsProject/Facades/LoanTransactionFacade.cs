﻿using LogsProject.Data;
using System;
using System.Collections.Generic;
/**
* @author jose.sanchezt@campusucc.edu.co
*
* @date - $time$ 
*/
namespace LogsProject.Facades
{
    public class LoanTransactionFacade
    {
        public static List<LoanTransaction> LoanTransactionList = new List<LoanTransaction>();

        public static List<LoanTransaction> createLoanTransactionList()
        {
            LoanTransactionList.Add(new LoanTransaction()
            {
                IdClient = "2341234213",
                Id = "1",
                TransactionDate = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                Comment = "Payment quota",
                LoanTransactionType = "A",
            });
            LoanTransactionList.Add(new LoanTransaction()
            {
                IdClient = "986462039",
                Id = "2",
                TransactionDate = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                Comment = "Payment quota",
                LoanTransactionType = "A",
            });
            LoanTransactionList.Add(new LoanTransaction()
            {
                IdClient = "986462039",
                Id = "3",
                TransactionDate = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                Comment = "Payment quota",
                LoanTransactionType = "B",
            });

            return LoanTransactionList;
        }
    }
}
