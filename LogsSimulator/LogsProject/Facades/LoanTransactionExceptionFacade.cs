﻿using LogsProject.Responses;
using System.Collections.Generic;
/**
* @author jose.sanchezt@campusucc.edu.co
*
* @date - $time$ 
*/
namespace LogsProject.Facades
{
    public class LoanTransactionExceptionFacade
    {
        public static List<TransactionsResponse> LoanTransactionList = new List<TransactionsResponse>();

        public List<TransactionsResponse> createLoanTransactionList()
        {
            LoanTransactionList = new List<TransactionsResponse>();
            var errors1 = new List<TransactionError>();
            errors1.Add(new TransactionError() { 
                Field = "",
            });

            LoanTransactionList.Add(new TransactionsResponse()
            {
                ResponseStatus = "ok",
                ResponseHeader = new ResponseHeader()
                {
                    Status = "200",
                },
                ResponseData = new ResponseData()
                {
                    Errors = errors1,
                }
            });

            var errors2 = new List<TransactionError>();
            errors2.Add(new TransactionError()
            {
                Code = "400",
                Message = "Bad Request",
            });

            LoanTransactionList.Add(new TransactionsResponse()
            {
                ResponseStatus = "fail",
                ResponseHeader = new ResponseHeader()
                {
                    Status = "400",
                },
                ResponseData = new ResponseData()
                {
                    Errors = errors2,
                }
            });

            var errors3 = new List<TransactionError>();
            errors3.Add(new TransactionError()
            {
                Code = "401",
                Message = "Unauthorized",
            });
            LoanTransactionList.Add(new TransactionsResponse()
            {
                ResponseStatus = "fail",
                ResponseHeader = new ResponseHeader()
                {
                    Status = "401",
                },
                ResponseData = new ResponseData()
                {
                    Errors = errors3,
                }
            });

            var errors4 = new List<TransactionError>();
            errors4.Add(new TransactionError()
            {
                Code = "403",
                Message = "Forbidden",
            });
            LoanTransactionList.Add(new TransactionsResponse()
            {
                ResponseStatus = "fail",
                ResponseHeader = new ResponseHeader()
                {
                    Status = "403",
                },
                ResponseData = new ResponseData()
                {
                    Errors = errors4,
                }
            });

            var errors5 = new List<TransactionError>();
            errors5.Add(new TransactionError()
            {
                Code = "404",
                Message = "Not found",
            });
            LoanTransactionList.Add(new TransactionsResponse()
            {
                ResponseStatus = "fail",
                ResponseHeader = new ResponseHeader()
                {
                    Status = "404",
                },
                ResponseData = new ResponseData()
                {
                    Errors = errors5,
                }
            });

            var errors6 = new List<TransactionError>();
            errors6.Add(new TransactionError()
            {
                Code = "405",
                Message = "Method Not Allowed",
            });
            LoanTransactionList.Add(new TransactionsResponse()
            {
                ResponseStatus = "fail",
                ResponseHeader = new ResponseHeader()
                {
                    Status = "405",
                },
                ResponseData = new ResponseData()
                {
                    Errors = errors6,
                }
            });

            var errors7 = new List<TransactionError>();
            errors7.Add(new TransactionError()
            {
                Code = "415",
                Message = "Unsupported Media Type",
            });
            LoanTransactionList.Add(new TransactionsResponse()
            {
                ResponseStatus = "fail",
                ResponseHeader = new ResponseHeader()
                {
                    Status = "415",
                },
                ResponseData = new ResponseData()
                {
                    Errors = errors7,
                }
            });

            var errors8 = new List<TransactionError>();
            errors8.Add(new TransactionError()
            {
                Code = "500",
                Field = "description",
                Message = "This Field is required",
            });
            LoanTransactionList.Add(new TransactionsResponse()
            {
                ResponseStatus = "fail",
                ResponseHeader = new ResponseHeader()
                {
                    Status = "500",
                },
                ResponseData = new ResponseData()
                {
                    Errors = errors8,
                }
            });



            var errors9 = new List<TransactionError>();
            errors9.Add(new TransactionError()
            {
                Code = "100",
                Field = "loan_transaction_date",
                Message = "This Field is required",
            });
            errors9.Add(new TransactionError()
            {
                Code = "100",
                Field = "loan_transaction_name",
                Message = "This Field is required",
            });
            LoanTransactionList.Add(new TransactionsResponse()
            {
                ResponseStatus = "fail",
                ResponseHeader = new ResponseHeader()
                {
                    Status = "500",
                },
                ResponseData = new ResponseData()
                {
                    Errors = errors8,
                }
            });

            var errors10 = new List<TransactionError>();
            errors10.Add(new TransactionError()
            {
                Code = "502",
                Message = "Bad Gateway",
            });
           
            LoanTransactionList.Add(new TransactionsResponse()
            {
                ResponseStatus = "fail",
                ResponseHeader = new ResponseHeader()
                {
                    Status = "502",
                },
                ResponseData = new ResponseData()
                {
                    Errors = errors10,
                }
            });

            var errors11 = new List<TransactionError>();
            errors11.Add(new TransactionError()
            {
                Code = "503",
                Message = "Service Unavailable",
            });

            LoanTransactionList.Add(new TransactionsResponse()
            {
                ResponseStatus = "fail",
                ResponseHeader = new ResponseHeader()
                {
                    Status = "503",
                },
                ResponseData = new ResponseData()
                {
                    Errors = errors11,
                }
            });

            return LoanTransactionList;
        }
    }
}
