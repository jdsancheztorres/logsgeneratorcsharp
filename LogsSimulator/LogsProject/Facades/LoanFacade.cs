﻿using LogsProject.Data;
using System;
using System.Collections.Generic;
/**
* @author jose.sanchezt@campusucc.edu.co
*
* @date - $time$ 
*/
namespace LogsProject.Facades
{
    /// <summary>
    /// Class <c>LoanFacade</c>Class used for loading request and responses parameters.
    /// </summary>
    public class LoanFacade
    {
        public static List<Loan> LoanList = new List<Loan>();

        public static void createLoanList()
        {
            LoanList = new List<Loan>();
            load2341234213Client();
            load986462039Client();
            load324726500Client();
        }

        public static void load2341234213Client()
        {
            Loan baseLoad = new Loan()
            {
                Id = "2341234213-1",
                IdClient = "2341234213",
                LoanDate = "03/01/2021",
                LoanValue = 35000000,
                LoanQuotas = 60,
                LoanRate = 19.56,
                LoanQuotaValue = 10210000,
                typeLoan = "Ordinary",
            };

            LoanList.Add(baseLoad);


            /**for (int month = 2; month < 12; month++)
            {
                string monthDef = month < 10 ? "0" + month : "" + month;
                Loan baseLoadCurrent = new Loan()
                {
                    Id = baseLoad.Id,
                    IdClient = baseLoad.IdClient,
                    LoanDate = "03/" + month + "/2021",
                    LoanValue = baseLoad.LoanValue - baseLoad.LoanQuotaValue,
                    LoanQuotas = baseLoad.LoanQuotas - 1,
                    LoanRate = baseLoad.LoanRate,
                    LoanQuotaValue = baseLoad.LoanQuotaValue,
                    typeLoan = baseLoad.typeLoan,
                };
                LoanList.Add(baseLoadCurrent);

                baseLoad.LoanValue = baseLoadCurrent.LoanValue;
                baseLoad.LoanQuotas = baseLoadCurrent.LoanQuotas;
                baseLoad.LoanRate = baseLoadCurrent.LoanRate;
                baseLoad.LoanQuotaValue = baseLoadCurrent.LoanQuotaValue;
            }**/
        }
        public static void load986462039Client()
        {
            Loan baseLoad = new Loan()
            {
                Id = "986462039-1",
                IdClient = "986462039",
                LoanDate = "06/01/2021",
                LoanValue = 25000000,
                LoanQuotas = 48,
                LoanRate = 19.56,
                LoanQuotaValue = 2154000,
                typeLoan = "Ordinary",
            };

            LoanList.Add(baseLoad);


            /**for (int month = 2; month < 12; month++)
            {
                string monthDef = month < 10 ? "0" + month : "" + month;
                Loan baseLoadCurrent = new Loan()
                {
                    Id = baseLoad.Id,
                    IdClient = baseLoad.IdClient,
                    LoanDate = "06/" + month + "/2021",
                    LoanValue = baseLoad.LoanValue - baseLoad.LoanQuotaValue,
                    LoanQuotas = baseLoad.LoanQuotas - 1,
                    LoanRate = baseLoad.LoanRate,
                    LoanQuotaValue = baseLoad.LoanQuotaValue,
                    typeLoan = baseLoad.typeLoan,
                };
                LoanList.Add(baseLoadCurrent);

                baseLoad.LoanValue = baseLoadCurrent.LoanValue;
                baseLoad.LoanQuotas = baseLoadCurrent.LoanQuotas;
                baseLoad.LoanRate = baseLoadCurrent.LoanRate;
                baseLoad.LoanQuotaValue = baseLoadCurrent.LoanQuotaValue;
            }**/
        }
        public static void load324726500Client()
        {
            Loan baseLoad = new Loan()
            {
                IdClient = "324726500",
                Id = "324726500-1",
                LoanDate = "01/01/2020",
                LoanValue = 180000000,
                LoanQuotas = 52,
                LoanRate = 20.2,
                LoanQuotaValue = 5671000,
                typeLoan = "Extraordinary",
            };

            LoanList.Add(baseLoad);


            /**for (int month = 2; month < 12; month++)
            {
                string monthDef = month < 10 ? "0" + month : "" + month;
                Loan baseLoadCurrent = new Loan()
                {
                    Id = baseLoad.Id,
                    IdClient = baseLoad.IdClient,
                    LoanDate = "01/" + month + "/2020",
                    LoanValue = baseLoad.LoanValue - baseLoad.LoanQuotaValue,
                    LoanQuotas = baseLoad.LoanQuotas - 1,
                    LoanRate = baseLoad.LoanRate,
                    LoanQuotaValue = baseLoad.LoanQuotaValue,
                    typeLoan = baseLoad.typeLoan,
                };
                LoanList.Add(baseLoadCurrent);

                baseLoad.LoanValue = baseLoadCurrent.LoanValue;
                baseLoad.LoanQuotas = baseLoadCurrent.LoanQuotas;
                baseLoad.LoanRate = baseLoadCurrent.LoanRate;
                baseLoad.LoanQuotaValue = baseLoadCurrent.LoanQuotaValue;
            }**/
        }

    }
}
