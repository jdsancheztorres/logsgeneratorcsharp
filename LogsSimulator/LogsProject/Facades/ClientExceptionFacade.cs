﻿using LogsProject.Responses;
using System.Collections.Generic;
/**
* @author jose.sanchezt@campusucc.edu.co
*
* @date - $time$ 
*/
namespace LogsProject.Facades
{
    public class ClientExceptionFacade
    {
        public static List<TransactionsResponse> ClientErrorList = new List<TransactionsResponse>();

        public static List<TransactionsResponse> createClientTransactionsResponseList()
        {
            ClientErrorList = new List<TransactionsResponse>();
            var errors1 = new List<TransactionError>();
            errors1.Add(new TransactionError()
            {
                Field = "",
            });

            ClientErrorList.Add(new TransactionsResponse()
            {
                ResponseStatus = "ok",
                ResponseHeader = new ResponseHeader()
                {
                    Status = "200",
                },
                ResponseData = new ResponseData()
                {
                    Errors = errors1,
                }
            });

            var errors2 = new List<TransactionError>();
            errors2.Add(new TransactionError()
            {
                Code = "400",
                Message = "Bad Request",
            });

            ClientErrorList.Add(new TransactionsResponse()
            {
                ResponseStatus = "fail",
                ResponseHeader = new ResponseHeader()
                {
                    Status = "400",
                },
                ResponseData = new ResponseData()
                {
                    Errors = errors2,
                }
            });

            var errors3 = new List<TransactionError>();
            errors3.Add(new TransactionError()
            {
                Code = "401",
                Message = "Unauthorized",
            });
            ClientErrorList.Add(new TransactionsResponse()
            {
                ResponseStatus = "fail",
                ResponseHeader = new ResponseHeader()
                {
                    Status = "401",
                },
                ResponseData = new ResponseData()
                {
                    Errors = errors3,
                }
            });

            var errors4 = new List<TransactionError>();
            errors4.Add(new TransactionError()
            {
                Code = "403",
                Message = "Forbidden",
            });
            ClientErrorList.Add(new TransactionsResponse()
            {
                ResponseStatus = "fail",
                ResponseHeader = new ResponseHeader()
                {
                    Status = "403",
                },
                ResponseData = new ResponseData()
                {
                    Errors = errors4,
                }
            });

            var errors5 = new List<TransactionError>();
            errors5.Add(new TransactionError()
            {
                Code = "404",
                Message = "Not found",
            });
            ClientErrorList.Add(new TransactionsResponse()
            {
                ResponseStatus = "fail",
                ResponseHeader = new ResponseHeader()
                {
                    Status = "404",
                },
                ResponseData = new ResponseData()
                {
                    Errors = errors5,
                }
            });

            var errors6 = new List<TransactionError>();
            errors6.Add(new TransactionError()
            {
                Code = "405",
                Message = "Method Not Allowed",
            });
            ClientErrorList.Add(new TransactionsResponse()
            {
                ResponseStatus = "fail",
                ResponseHeader = new ResponseHeader()
                {
                    Status = "405",
                },
                ResponseData = new ResponseData()
                {
                    Errors = errors6,
                }
            });

            var errors7 = new List<TransactionError>();
            errors7.Add(new TransactionError()
            {
                Code = "415",
                Message = "Unsupported Media Type",
            });
            ClientErrorList.Add(new TransactionsResponse()
            {
                ResponseStatus = "fail",
                ResponseHeader = new ResponseHeader()
                {
                    Status = "415",
                },
                ResponseData = new ResponseData()
                {
                    Errors = errors7,
                }
            });

            var errors71 = new List<TransactionError>();
            errors71.Add(new TransactionError()
            {
                Code = "415",
                Message = "Unsupported Media Type",
            });
            ClientErrorList.Add(new TransactionsResponse()
            {
                ResponseStatus = "fail",
                ResponseHeader = new ResponseHeader()
                {
                    Status = "500",
                },
                ResponseData = new ResponseData()
                {
                    Errors = errors71,
                }
            });

            var errors8 = new List<TransactionError>();
            errors8.Add(new TransactionError()
            {
                Code = "201",
                Field = "ClientType",
                Message = "This Field is incorrect",
            });
            ClientErrorList.Add(new TransactionsResponse()
            {
                ResponseStatus = "fail",
                ResponseHeader = new ResponseHeader()
                {
                    Status = "500",
                },
                ResponseData = new ResponseData()
                {
                    Errors = errors8,
                }
            });



            var errors9 = new List<TransactionError>();
            errors9.Add(new TransactionError()
            {
                Code = "200",
                Field = "FirstName",
                Message = "This Field is required",
            });
            errors9.Add(new TransactionError()
            {
                Code = "200",
                Field = "LastName",
                Message = "This Field is required",
            });
            ClientErrorList.Add(new TransactionsResponse()
            {
                ResponseStatus = "fail",
                ResponseHeader = new ResponseHeader()
                {
                    Status = "500",
                },
                ResponseData = new ResponseData()
                {
                    Errors = errors8,
                }
            });

            var errors10 = new List<TransactionError>();
            errors10.Add(new TransactionError()
            {
                Code = "502",
                Message = "Bad Gateway",
            });

            ClientErrorList.Add(new TransactionsResponse()
            {
                ResponseStatus = "fail",
                ResponseHeader = new ResponseHeader()
                {
                    Status = "502",
                },
                ResponseData = new ResponseData()
                {
                    Errors = errors10,
                }
            });

            var errors11 = new List<TransactionError>();
            errors11.Add(new TransactionError()
            {
                Code = "503",
                Message = "Service Unavailable",
            });

            ClientErrorList.Add(new TransactionsResponse()
            {
                ResponseStatus = "fail",
                ResponseHeader = new ResponseHeader()
                {
                    Status = "503",
                },
                ResponseData = new ResponseData()
                {
                    Errors = errors11,
                }
            });

            return ClientErrorList;
        }
    }
}

