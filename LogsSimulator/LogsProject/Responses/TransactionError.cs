﻿/**
* @author jose.sanchezt@campusucc.edu.co
*
* @date - $time$ 
*/
namespace LogsProject.Responses
{
    public class TransactionError
    {
        public string Field { get; set; }
        public string Code { get; set; }
        public string Message { get; set; }
    }
}
