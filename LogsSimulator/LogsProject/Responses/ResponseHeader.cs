﻿/**
* @author jose.sanchezt@campusucc.edu.co
*
* @date - $time$ 
*/
namespace LogsProject.Responses
{
    public class ResponseHeader
    {
        public float Now { get; set; }
        public string Status { get; set; }
        public string RequestId { get; set; }
    }
}
