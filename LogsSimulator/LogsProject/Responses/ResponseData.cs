﻿using System.Collections.Generic;
/**
* @author jose.sanchezt@campusucc.edu.co
*
* @date - $time$ 
*/
namespace LogsProject.Responses
{
    public class ResponseData
    {
        private List<TransactionError> errors = new List<TransactionError>();

        public List<TransactionError> Errors
        {
            // no setter
            get { return errors; }

            set { errors = errors; }
        }
    }
}
