﻿/**
* @author jose.sanchezt@campusucc.edu.co
*
* @date - $time$ 
*/
namespace LogsProject.Responses
{
    public class TransactionsResponse
    {
        public string ResponseStatus { get; set; }
        public ResponseHeader ResponseHeader { get; set; }
        public ResponseData ResponseData { get; set; }
    }
}
