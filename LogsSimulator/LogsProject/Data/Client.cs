﻿/**
* @author jose.sanchezt@campusucc.edu.co
*
* @date - $time$ 
*/
namespace LogsProject.Data
{
    public class Client
    {
        public string Id { get; set; }
        public string IdType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ClientType { get; set; }
        public string AccountNumber { get; set; }
    }
}
