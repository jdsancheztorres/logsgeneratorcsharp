﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/**
* @author jose.sanchezt@campusucc.edu.co
*
* @date - $time$ 
*/
namespace LogsProject.Data
{
    public class Loan
    {
        public string Id { get; set; }
        public string IdClient { get; set; }
        public string LoanDate{ get; set; }
        public double LoanValue { get; set; }
        public int LoanQuotas { get; set; }
        public double LoanRate { get; set; }
        public double LoanQuotaValue { get; set; }
        public string typeLoan { get; set; }

    }
}
