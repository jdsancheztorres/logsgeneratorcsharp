﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/**
* @author jose.sanchezt@campusucc.edu.co
*
* @date - $time$ 
*/
namespace LogsProject.Data
{
    public class LoanTransaction
    {
        public string Id { get; set; }

        public string IdClient { get; set; }
        public string LoanTransactionType { get; set; }
        public string TransactionDate { get; set; }
        public double TransactionValue { get; set; }
        public string Comment{ get; set; }
    }
}
