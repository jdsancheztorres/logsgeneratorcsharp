﻿using LogsProject.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
/**
* @author jose.sanchezt@campusucc.edu.co
*
* @date - $time$ 
*/
namespace LogsProject.Requests
{
    public class LoanRequest
    {
        [JsonPropertyName("addLoans")]
        public IEnumerable<Loan> AddLoans { get; set; } = new List<Loan>();

        [JsonPropertyName("updateLoans")]
        public IEnumerable<Loan> UpdateLoans { get; set; } = new List<Loan>();

        [JsonPropertyName("deleteLoans")]
        public IEnumerable<Loan> DeleteLoans { get; set; } = new List<Loan>();
    }
}
