﻿using LogsProject.Data;
using System.Collections.Generic;
using System.Text.Json.Serialization;
/**
* @author jose.sanchezt@campusucc.edu.co
*
* @date - $time$ 
*/
namespace LogsProject.Requests
{
    public class LoanTransactionRequest
    {
        [JsonPropertyName("addLoanTransactions")]
        public IEnumerable<LoanTransaction> AddLoanTransactions { get; set; } = new List<LoanTransaction>();
    }
}
