﻿using LogsProject.Data;
using System.Collections.Generic;
using System.Text.Json.Serialization;
/**
* @author jose.sanchezt@campusucc.edu.co
*
* @date - $time$ 
*/
namespace LogsProject.Requests
{
    public class ClientRequest
    {
        [JsonPropertyName("addClients")]
        public IEnumerable<Client> AddClients { get; set; } = new List<Client>();

        [JsonPropertyName("updateClients")]
        public IEnumerable<Client> UpdateClients { get; set; } = new List<Client>();

        [JsonPropertyName("deleteClients")]
        public IEnumerable<Client> DeleteClients { get; set; } = new List<Client>();
    }
}
