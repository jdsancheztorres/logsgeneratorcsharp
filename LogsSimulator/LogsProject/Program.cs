﻿using log4net.Repository;
using LogsProject.Jobs;
using Microsoft.Extensions.Hosting;
using Quartz;
using System.IO;
using System.Reflection;
/**
* @author jose.sanchezt@campusucc.edu.co
*
* @date - $time$ 
*/
namespace LogsProject
{
    class Program
    {
        private string[] initialMethodNameList = new string[100];

        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();
            ILoggerRepository repository = log4net.LogManager.GetRepository(Assembly.GetEntryAssembly());
            var fileInfo = new FileInfo(@"App.config");
            log4net.Config.XmlConfigurator.Configure(repository, fileInfo);
            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
            .ConfigureServices((hostContext, services) =>
            {
                services.AddQuartz(q =>
                {
                    q.UseMicrosoftDependencyInjectionScopedJobFactory();
                    AddClientJob(q);
                    AddLoanJob(q);
                    AddLoanTransactionJob(q);
                });
                services.AddQuartzHostedService(q => q.WaitForJobsToComplete = true);

            });


        private static void AddClientJob(IServiceCollectionQuartzConfigurator q)
        {

            // Create a "key" for the job
            var jobKey = new JobKey("ClientJob");
            // Register the job with the DI container
            q.AddJob<ClientJob>(opts => opts.WithIdentity(new JobKey("ClientJob")));
            // Create a trigger for the job
            q.AddTrigger(opts => opts
                .ForJob(jobKey) // link to the HelloWorldJob
                .WithIdentity("ClientJob-trigger") // give the trigger a unique name
                .WithCronSchedule("0/5 * * * * ?")); // run every 5 seconds
        }

        private static void AddLoanJob(IServiceCollectionQuartzConfigurator q)
        {

            // Create a "key" for the job
            var jobKey = new JobKey("LoanJob");
            // Register the job with the DI container
            q.AddJob<LoanJob>(opts => opts.WithIdentity(new JobKey("LoanJob")));
            // Create a trigger for the job
            q.AddTrigger(opts => opts
                .ForJob(jobKey) // link to the HelloWorldJob
                .WithIdentity("LoanJob-trigger") // give the trigger a unique name
                .WithCronSchedule("0/5 * * * * ?")); // run every 3 seconds
        }
        private static void AddLoanTransactionJob(IServiceCollectionQuartzConfigurator q)
        {

            // Create a "key" for the job
            var jobKey = new JobKey("LoanTransactionJob");
            // Register the job with the DI container
            q.AddJob<LoanTransactionJob>(opts => opts.WithIdentity(new JobKey("LoanTransactionJob")));
            // Create a trigger for the job
            q.AddTrigger(opts => opts
                .ForJob(jobKey) // link to the HelloWorldJob
                .WithIdentity("LoanTransactionJob-trigger") // give the trigger a unique name
                .WithCronSchedule("0/5 * * * * ?")); // run every 15 minutes 
        }
    }
}
