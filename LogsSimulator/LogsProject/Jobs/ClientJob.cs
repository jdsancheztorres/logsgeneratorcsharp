﻿using log4net;
using LogsProject.Data;
using LogsProject.Facades;
using LogsProject.Requests;
using LogsProject.Responses;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
/**
* @author jose.sanchezt@campusucc.edu.co
*
* @date - $time$ 
*/
namespace LogsProject.Jobs
{
    [DisallowConcurrentExecution]
    public class ClientJob : CommonJob, IJob
    {
        private readonly ILog Logger = LogManager.GetLogger(typeof(ClientJob));

        public Task Execute(IJobExecutionContext context)
        {
            String userInfo = Environment.MachineName + " " + Environment.UserName + " " + Environment.OSVersion + " ";
            ClientRequest request = createClientRequest();
            Logger.Info(userInfo + "- ClientTask.Api.Controllers --- Start");
            Logger.Info(userInfo + "----- Handling POST request: Controller associated to the client_service componente - Value:" + this.convertDataToJson(request) + "");
            Logger.Debug(userInfo + "- ClientTask.Api.Infrastructure.Services.client -- Start");
            Logger.Debug(userInfo + "----Inserting " + request.AddClients.Count() + " records ");
            Logger.Debug(userInfo + "----Updating " + request.UpdateClients.Count() + " records ");
            Logger.Debug(userInfo + "---- Deleting " + request.DeleteClients.Count()  + " records: ");
            var response = createClientError();
            if (response.ResponseStatus.Equals("fail"))
            {
                Logger.Error(userInfo + "- Error: This component has identified an error in the POST execution: " + this.convertDataToJson(response) + "")
;
            }
            else
            {
                Logger.Info(userInfo + "- ClientTask.Api.Infrastructure.Services.client -- response OK:" + this.convertDataToJson(response) + "");
                Logger.Info(userInfo + "- ClientTask.Api.Infrastructure.Services.client -- End");
                Logger.Info(Environment.OSVersion + "ClientTask.Api.Controllers --- End");
            }

            return Task.CompletedTask;
        }

        private  ClientRequest createClientRequest()
        {
            Random random = new Random();
            ClientFacade.CreateClientList();
            Client client = ClientFacade.ClientList[random.Next(0, ClientFacade.ClientList.Count)];
            IEnumerable<Client> addClientsList = new List<Client>();
            List<Client> clientList =  new List<Client>();
            clientList.Add(client);
            addClientsList = (IEnumerable<Client>)clientList;
            ClientRequest request = new ClientRequest()
            {
                AddClients = addClientsList,
            };

            return request;
        }

        private TransactionsResponse createClientError()
        {
            Random random = new Random();
            ClientExceptionFacade.createClientTransactionsResponseList();
            int indexRandom = random.Next(0, LoanExceptionFacade.LoanList.Count);
            int finalIndex = (indexRandom % 9 == 0 ? indexRandom : 0);
            var transactionsResponse = ClientExceptionFacade
                .ClientErrorList[finalIndex];
            return transactionsResponse;
        }

        private String convertDataToJson(ClientRequest request)
        {
            var opt = new JsonSerializerOptions() { WriteIndented = true };
            string strJson = JsonSerializer.Serialize<ClientRequest>(request, opt);
            return strJson;
        }
    }
}
