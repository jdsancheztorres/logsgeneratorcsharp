﻿using log4net;
using LogsProject.Data;
using LogsProject.Facades;
using LogsProject.Requests;
using LogsProject.Responses;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
/**
* @author jose.sanchezt@campusucc.edu.co
*
* @date - $time$ 
*/
namespace LogsProject.Jobs
{
    [DisallowConcurrentExecution]
    public class LoanTransactionJob : CommonJob, IJob
    {
        private readonly ILog logger = LogManager.GetLogger(typeof(LoanTransactionJob));

        public Task Execute(IJobExecutionContext context) 
        {
            String userInfo = Environment.MachineName + " " + Environment.UserName + " " + Environment.OSVersion;
            LoanTransactionRequest request = createLoanTransactionRequest();
            logger.Info(userInfo + "- LoanTransactionTask.Api.Controllers --- Start");
            logger.Info(userInfo + "----- Handling POST request: Controller associated to the loanTransaction_service componente - Value:" + this.convertDataToJson(request) + "");
            logger.Debug(userInfo + "- LoanTransactionTask.Api.Infrastructure.Services.LoanTransaction -- Start");
            logger.Debug(userInfo + "----Inserting " + request.AddLoanTransactions.Count() + " records ");
            var response = createTransactionError();
            if (response.ResponseStatus.Equals("fail"))
            {
                logger.Error(userInfo + "- Error: This component has identified an error in the POST execution: " + this.convertDataToJson(response) + "")
;            }
            else
            {
                logger.Info(userInfo + "- LoanTransactionTask.Api.Infrastructure.Services.LoanTransaction -- response OK:" + this.convertDataToJson(response) + "");
                logger.Info(userInfo + "- LoanTransactionTask.Api.Infrastructure.Services.LoanTransaction -- End");
                logger.Info(Environment.OSVersion + "- LoanTransactionTask.Api.Controllers --- End");
            }
            
            return Task.CompletedTask;
        }

        private LoanTransactionRequest createLoanTransactionRequest()
        {
            Random random = new Random();
            LoanTransactionFacade.createLoanTransactionList();
            LoanTransaction LoanTransaction = LoanTransactionFacade.LoanTransactionList[random.Next(0, LoanTransactionFacade.LoanTransactionList.Count)];
            IEnumerable<LoanTransaction> addLoanTransactionsList = new List<LoanTransaction>();
            List<LoanTransaction> loanTransactionList = new List<LoanTransaction> ();
            loanTransactionList.Add(LoanTransaction);
            addLoanTransactionsList = (IEnumerable<LoanTransaction>)loanTransactionList;
            LoanTransactionRequest request = new LoanTransactionRequest()
            {
                AddLoanTransactions = addLoanTransactionsList
            };

            return request;
        }

        private TransactionsResponse createTransactionError()
        {
            Random random = new Random();
            LoanExceptionFacade.createClientTransactionsResponseList();
            int indexRandom = random.Next(0, LoanExceptionFacade.LoanList.Count);
            int finalIndex = (indexRandom % 9 == 0 ? indexRandom : 0);
            var transactionsResponse = LoanExceptionFacade.
                LoanList[finalIndex];
            return transactionsResponse;
        }


        private String convertDataToJson(LoanTransactionRequest request)
        {
            var opt = new JsonSerializerOptions() { WriteIndented = true };
            string strJson = JsonSerializer.Serialize<LoanTransactionRequest>(request, opt);
            return strJson;
        }
    }
}
