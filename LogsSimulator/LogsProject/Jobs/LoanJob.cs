﻿using log4net;
using LogsProject.Data;
using LogsProject.Facades;
using LogsProject.Requests;
using LogsProject.Responses;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
/**
* @author jose.sanchezt@campusucc.edu.co
*
* @date - $time$ 
*/
namespace LogsProject.Jobs
{
    [DisallowConcurrentExecution]
    public class LoanJob : CommonJob, IJob
    {
        private readonly ILog Logger = LogManager.GetLogger(typeof(LoanJob));

        public Task Execute(IJobExecutionContext context)
        {
            String userInfo = Environment.MachineName + " " + Environment.UserName + " " + Environment.OSVersion + " ";
            LoanRequest request = createLoanRequest();
            Logger.Info(userInfo + "- LoanTask.Api.Controllers --- Start");
            Logger.Debug(userInfo + "----- Handling POST request: Controller associated to the loan_service component - Value:" + this.convertDataToJson(request));
            Logger.Debug(userInfo + "- LoanTask.Api.Infrastructure.Services.Loan -- Start");
            Logger.Debug(userInfo + "----Inserting " + request.AddLoans.Count() + " records.");
            Logger.Debug(userInfo + "----Updating " + request.UpdateLoans.Count()  + " records.");
            Logger.Debug(userInfo + "---- Deleting " + request.DeleteLoans.Count()  + " records.");
            var response = createLoanError();
            if (response.ResponseStatus.Equals("fail"))
            {
                Logger.Error(userInfo + "- Error: This component has identified an error in the POST execution: " + this.convertDataToJson(response) + "")
;
            }
            else
            {
                Logger.Info(userInfo + "- LoanTask.Api.Infrastructure.Services.Loan -- response OK:" + this.convertDataToJson(response) + "");
                Logger.Info(userInfo + "- LoanTask.Api.Infrastructure.Services.Loan -- End");
                Logger.Info(userInfo + "LoanTask.Api.Controllers --- End");
            }

            return Task.CompletedTask;
        }

        private LoanRequest createLoanRequest()
        {
            Random random = new Random();
            LoanFacade.createLoanList();
            Loan loan = LoanFacade.LoanList[random.Next(0, LoanFacade.LoanList.Count)];
            IEnumerable<Loan> addLoansList = new List<Loan>();
            List<Loan> loanList = new List<Loan>();
            LoanFacade.LoanList.Add(loan);
            addLoansList = (IEnumerable<Loan>)LoanFacade.LoanList;
            LoanRequest request = new LoanRequest()
            {
                AddLoans = addLoansList
            };

            return request;
        }
        private TransactionsResponse createLoanError()
        {
            Random random = new Random();
            int indexRandom = random.Next(0, LoanExceptionFacade.LoanList.Count);
            int finalIndex = (indexRandom % 9 == 0 ? indexRandom : 0);
            var transactionsResponse = LoanExceptionFacade.
                LoanList[finalIndex];
            return transactionsResponse;
        }

        private String convertDataToJson(LoanRequest request)
        {
            var opt = new JsonSerializerOptions() { WriteIndented = true };
            string strJson = JsonSerializer.Serialize<LoanRequest>(request, opt);
            return strJson;
        }
    }
}
