﻿using LogsProject.Responses;
using System;
using System.Text.Json;
/**
* @author jose.sanchezt@campusucc.edu.co
*
* @date - $time$ 
*/
namespace LogsProject.Jobs
{
    public abstract class CommonJob
    {
        protected String convertDataToJson(TransactionsResponse request)
        {
            var opt = new JsonSerializerOptions() { WriteIndented = true };
            string strJson = JsonSerializer.Serialize<TransactionsResponse>(request, opt);
            return strJson;
        }
    }
}
